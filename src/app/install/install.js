angular.module("install",[])
.controller("InstallCtrl", ["$scope", "$window", "$rootScope","$timeout","$interval",
  function ($scope, $window, $rootScope, $timeout, $interval){
    
    $(".content").css("height", $rootScope.contentHeight);

    var showError = function(){
      console.log("error");
      $scope.error = true;
    }
    var currentProgress;
    var updateStatus = function(){
      var status = $rootScope.installation.getStatus();
      console.log(status.status + ":" + status.description);
      $scope.currentStep = status.description;
      if ($scope.currentStep === 'copying_filesystem') {
        if (!currentProgress) {
          currentProgress = 20;
        }
        if (currentProgress < 80) {
          currentProgress += 0.5;
        }
        status.progress = currentProgress;
        console.log(status.progress);
      }
      $scope.progressBarWidth = status.progress;
      if (status.status > 1) {
        $interval.cancel(statusUpdater);
        if (status.status == 2) {
          showError();
        } else {
          $rootScope.next();
        }
      }
    }
    $scope.loadingDot = "";
    $interval(function(){
      if ($scope.loadingDot.length === 8) {
        $scope.loadingDot = "";
      } else {
        $scope.loadingDot += " .";
      }
    }, 500);

    $scope.params = "";
    if ($rootScope.installationData.secureInstall) {
        $rootScope.installationData.partition = 0;
        $scope.params += "&secureInstall=" + $rootScope.installationData.secureInstall;
        $scope.params += "&secureInstallPassphrase=" + $rootScope.installationData.secureInstallPassphrase;
        $scope.params += "&rootPassword=" + $rootScope.installationData.rootPassword;
    }
    $scope.params += "&partition=" + $rootScope.installationData.partition;
    $scope.params += "&device=" + $rootScope.installationData.device;
    $scope.params += "&device_path=" + $rootScope.installationData.device_path;
    $scope.params += "&hostname=" + $rootScope.installationData.hostname;
    $scope.params += "&username=" + $rootScope.installationData.username;
    $scope.params += "&fullname=" + $rootScope.installationData.fullname;
    $scope.params += "&password=" + $rootScope.installationData.password;
    $scope.params += "&language=" + $rootScope.installationData.language;
    $scope.params += "&timezone=" + $rootScope.installationData.timezone;
    $scope.params += "&keyboard=" + $rootScope.installationData.keyboard;
    $scope.params += "&autologin=" + $rootScope.installationData.autologin;
    $scope.params += "&advancedMode=" + $rootScope.advancedPartition;
    if ($rootScope.advancedPartition) {
        $scope.params += "&steps=" + $rootScope.partitionSteps;
    }
    console.log($rootScope.installationData);
    console.log($scope.params);
    
    // give time for view transition
    if (window.Installation) {
      Installation.setTimezone($rootScope.installationData.timezone);
      $timeout(function(){
        console.log($scope.params);
        $rootScope.installation = new Installation($scope.params);
        $rootScope.installation.start();
        $scope.currentStep = "";
        statusUpdater = $interval(updateStatus, 1000);
      }, 1000);
    }

}])
