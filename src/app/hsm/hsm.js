angular.module("hsm",[])
.controller("HSMCtrl", ["$scope", "$window", "$rootScope", "$translate",
  function ($scope, $window, $rootScope, $translate){
    
    $(".content").css("height", $rootScope.contentHeight);
    
    var currentLang = '';
    if (window.Installation) {
      currentLang = Installation.getCurrentLanguage();
    }
    for (var i in $rootScope.languages) {
      if ($rootScope.languages[i].id && $rootScope.languages[i].id == currentLang) {
        $rootScope.setLanguage($rootScope.languages[i]);
      }
    }
    
    $scope.pin = '';
    $scope.HSMDetail = {}; 
    $scope.loading = false;

    $scope.loadHSM = function() {
      $scope.HSMDetail = {};
      $scope.cardDoesntExist = false;
      $scope.pinIncorrect = false;
      // This function return HSM vendor detail or empty string, which mean the HSM doesn't exist, not supported or contains no valid certificate
      // If it return a value, then the public key has been extracted successfuly and has encrypt the passphrase
      if (!$scope.pin || ($scope.pin && $scope.pin.length < 1)) {
        return alert('Please enter the HSM PIN');
      }
      var value = ''
      if (window.Installation) {
        value = Installation.loadHSM($scope.pin);
      }
      console.log(value);
      $scope.loading = true;
        $scope.HSMDetail = JSON.parse(value);
      try {
      } catch (err) {
        $scope.HSMDetail = {};
      }
      $scope.loading = false;
      if ($scope.HSMDetail.error) {
        $scope.cardDoesntExist = true;
        return;
      }
      if ($scope.HSMDetail && !$scope.HSMDetail.subject) {
        $scope.pinIncorrect = true;
        $scope.pin = '';
        return;
      }
      $scope.cardDoesntExist = false;
      $scope.pinIncorrect = false;
    }
    
    $scope.shutdown = function(){
      console.log("shutdown");
      Installation.shutdown();
    };
      
    if ($rootScope.quickDebug) {
      setTimeout(function(){
        $rootScope.next();
      }, 1000);
    }
}])
