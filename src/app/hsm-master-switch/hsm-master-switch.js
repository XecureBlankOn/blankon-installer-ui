angular.module("hsm-master-switch",[])
.controller("HSMMasterSwitchCtrl", ["$scope", "$window", "$rootScope", "$translate",
  function ($scope, $window, $rootScope, $translate){
    
    $(".content").css("height", $rootScope.contentHeight);
    
    var currentLang = '';
    if (Installation) {
      Installation.getCurrentLanguage();
    }
    for (var i in $rootScope.languages) {
      if ($rootScope.languages[i].id && $rootScope.languages[i].id == currentLang) {
        $rootScope.setLanguage($rootScope.languages[i]);
      }
    }
    
    $scope.pin = '';
    $scope.HSMDetail = {}; 
    $scope.loading = false;

    $scope.switchHSM = function() {
      $scope.HSMDetail = {};
      $scope.wrongToken = false;
      $scope.cardDoesntExist = false;
      $scope.pinIncorrect = false;
      // This function return HSM vendor detail or empty string, which mean the HSM doesn't exist, not supported or contains no valid certificate
      // If it return a value, then the public key has been extracted successfuly and has encrypt the passphrase
      if (!$scope.pin || ($scope.pin && $scope.pin.length < 1)) {
        return alert('Please enter the HSM PIN');
      }
      var value = ''
      if (Installation) {
        value = Installation.switchHSM($scope.pin);
      }
      console.log(value);
      $scope.loading = true;
        $scope.HSMDetail = JSON.parse(value);
      try {
      } catch (err) {
        $scope.HSMDetail = {};
        $scope.pinIncorrect = true;
      }
      $scope.loading = false;
      if ($scope.HSMDetail.error) {
        if ($scope.HSMDetail.error === 'Wrong PIN') {
          $scope.pinIncorrect = true;
        } else if ($scope.HSMDetail.error === 'Wrong Token') {
          $scope.wrongToken = true;
        } else {
          $scope.cardDoesntExist = true;
        }
        return;
      }
      $scope.cardDoesntExist = false;
      $scope.pinIncorrect = false;
      $rootScope.next();
    }
      
    if ($rootScope.quickDebug) {
      setTimeout(function(){
        $rootScope.next();
      }, 1000);
    }
}])
