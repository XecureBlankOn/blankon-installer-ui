angular.module("postInstallDone",[])
.controller("PostInstallDoneCtrl", ["$scope", "$window", "$rootScope", 
  function ($scope, $window, $rootScope){
    
    $(".content").css("height", $rootScope.contentHeight);
    $scope.done = false;
    $scope.logout = function(){
      if (!$scope.done) {
        return;
      }
      Installation.logout();
    };
    $scope.reboot = function(){
      console.log("reboot");
      Installation.reboot();
    };
    // Give time for transition
    setTimeout(function(){
      Installation.securePostInstallConfig(
        $rootScope.installationData.rootPassword, 
        $rootScope.installationData.hostname,
        $rootScope.installationData.fullname,
        $rootScope.installationData.username,
        $rootScope.installationData.password
      );
      $scope.done = true;
      $scope.$apply();
    }, 2000)    
}])
