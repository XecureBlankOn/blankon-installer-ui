'use strict';
angular.module('Biui', [
  "ui.router",
  "ngAnimate",
  "pascalprecht.translate",
  "angularAwesomeSlider",
  "html",
  "mm.foundation",
  "debounce",
  "hello",
  "hsm-master",
  "hsm-master-switch",
  "hsm",
  "timezone",
  "partition",
  "summary",
  "install",
  "done",
  "encryption",
  "root",
  "user",
  "postInstallDone"
])
.config(function ($translateProvider) {
  $translateProvider.translations("en_US.utf8", en);
  $translateProvider.translations("id_ID.utf8", id);
  $translateProvider.preferredLanguage("en_US.utf8");
})
.config(function($stateProvider) {
  $stateProvider
  .state("hello", {
      url: "/hello",
      controller: "HelloCtrl",
      templateProvider: function($templateCache) {
        return $templateCache.get("hello/hello.html");
      }
    }
  )
})
.config(function($stateProvider) {
  $stateProvider
  .state("hsm-master", {
      url: "/hsm-master",
      controller: "HSMMasterCtrl",
      templateProvider: function($templateCache) {
        return $templateCache.get("hsm-master/hsm-master.html");
      }
    }
  )
})
.config(function($stateProvider) {
  $stateProvider
  .state("hsm-master-switch", {
      url: "/hsm-master-switch",
      controller: "HSMMasterSwitchCtrl",
      templateProvider: function($templateCache) {
        return $templateCache.get("hsm-master-switch/hsm-master-switch.html");
      }
    }
  )
})
.config(function($stateProvider) {
  $stateProvider
  .state("hsm", {
      url: "/hsm",
      controller: "HSMCtrl",
      templateProvider: function($templateCache) {
        return $templateCache.get("hsm/hsm.html");
      }
    }
  )
})
.config(function($stateProvider) {
  $stateProvider
  .state("timezone", {
      url: "/timezone",
      controller: "TimezoneCtrl",
      templateUrl: "timezone.html"
    }
  )
})
.config(function($stateProvider) {
  $stateProvider
  .state("partition", {
      url: "/partition",
      controller: "PartitionCtrl",
      templateProvider: function($templateCache) {
        return $templateCache.get("partition/partition.html");
      }
    }
  )
})
.config(function($stateProvider) {
  $stateProvider
  .state("root", {
      url: "/root",
      controller: "RootCtrl",
      templateProvider: function($templateCache) {
        return $templateCache.get("root/root.html");
      }
    }
  )
})
.config(function($stateProvider) {
  $stateProvider
  .state("user", {
      url: "/user",
      controller: "UserCtrl",
      templateProvider: function($templateCache) {
        return $templateCache.get("user/user.html");
      }
    }
  )
})
.config(function($stateProvider) {
  $stateProvider
  .state("summary", {
      url: "/summary",
      controller: "SummaryCtrl",
      templateProvider: function($templateCache) {
        return $templateCache.get("summary/summary.html");
      }
    }
  )
})
.config(function($stateProvider) {
  $stateProvider
  .state("install", {
      url: "/install",
      controller: "InstallCtrl",
      templateProvider: function($templateCache) {
        return $templateCache.get("install/install.html");
      }
    }
  )
})
.config(function($stateProvider) {
  $stateProvider
  .state("done", {
      url: "/done",
      controller: "DoneCtrl",
      templateProvider: function($templateCache) {
        return $templateCache.get("done/done.html");
      }
    }
  )
})
.config(function($stateProvider) {
  $stateProvider
  .state("postInstallDone", {
      url: "/post-install-done",
      controller: "PostInstallDoneCtrl",
      templateProvider: function($templateCache) {
        return $templateCache.get("post-install-done/post-install-done.html");
      }
    }
  )
})

.run([ "$rootScope", "$state", "$stateParams", "$timeout", "$location", "$translate",
  function ($rootScope, $state, $stateParams, $timeout, $location, $translate) {
    $rootScope.quickDebug = false;
    $rootScope.isEfi = false;
    $translate.use("enUS");
    $rootScope.steps = [
      {
        seq : 0,
        step : 1,
        name : "Introduction",
        path : "hello"
      },
      {
        seq : 1,
        step : 2,
        name : "HSMMaster",
        path : "hsm-master"
      },
      {
        seq : 2,
        step : 3,
        name : "Timezone",
        path : "timezone"
      },
      {
        seq : 3,
        step : 4,
        name : "Installation Target",
        path : "partition"
      },

      {
        seq : 4,
        step : 5,
        name : "Installation Summary",
        path : "summary"
      },
      {
        seq : 5,
        step : 6,
        name : "Installing...",
        path : ""
      },
      {
        seq : 6,
        step : 7,
        name : "Finish`",
        path : ""
      },
      {
        seq : 7,
        step : 8,
        name : "HSM",
        path : "hsm"
      },
      {
        seq : 8,
        step : 9,
        name : "Root Password",
        path : "root"
      },

      {
        seq : 9,
        step : 10,
        name : "Personalization",
        path : "user"
      },
      {
        seq : 10,
        step : 11,
        name : "HSM Master Switch",
        path : "hsm-master-switch"
      },
      {
        seq : 11,
        step : 12,
        name : "Post Install Done",
        path : "postInstallDone"
      },

    ]

    $rootScope.goStep = function (seq) {
      if (seq < 4) {
        $rootScope.currentState = seq;
        $location.path($rootScope.steps[seq].path);
      }
    }

    $rootScope.installationData = {};

    $rootScope.states = [
      "hello",
      "hsm-master",
      "timezone",
      "partition",
      "summary",
      "install",
      "done",
      "hsm",
      "root",
      "user",
      "hsm-master-switch",
      "postInstallDone",
    ]

    $rootScope.advancedMode = function() {
      $rootScope.simplePartitioning = false;
    }
    $rootScope.simpleMode = function() {
      $rootScope.simplePartitioning = true;
    }
    $rootScope.currentState = 0;
    $rootScope.simplePartitioning = true;
    $rootScope.back = false;
    $rootScope.forward = true;
    if (window.Installation) {
      $rootScope.isSecurePostInstall = (Installation.isSecurePostInstall() == 'true') ? true : false;
    }
    if ($rootScope.isSecurePostInstall) {
      $rootScope.currentState = 7;
    } else {
      // Set required values for pre installation steps
      var random = Math.random().toString(36).substring(7);
      $rootScope.installationData.rootPassword = random;
      $rootScope.installationData.rootPasswordRepeat = random
      $rootScope.installationData.autologin = true;
      $rootScope.installationData.enforceStrongPassword = false;
      $rootScope.installationData.hostname = random
      $rootScope.installationData.fullname = random
      $rootScope.installationData.username = random
      $rootScope.installationData.password = random
      $rootScope.installationData.secureInstallPassphrase = random;
      $rootScope.installationData.secureInstallPassphraseRepeat = random;
    }

    // initiate partition state early
    $rootScope.partitionState = {
      mountPoint: {},
      stateIndex : 0,
      history : [],
    }

    $rootScope.next = function() {
      $rootScope.back = false;
      $rootScope.forward = true;
      $timeout(function(){
        if ($rootScope.currentState + 1 < $rootScope.states.length) {
          $rootScope.currentState ++;

          var state = $rootScope.states[$rootScope.currentState];
          console.log(state);
          $state.go(state);
        }
      }, 100);
    }

    $rootScope.previous = function() {
      $rootScope.back = true;
      $rootScope.forward = false;
      console.log($rootScope.back);
      $timeout(function(){
        if ($rootScope.currentState - 1 >= 0) {
          $rootScope.currentState--;
          $state.go($rootScope.states[$rootScope.currentState]);
        }
      }, 100);
    }
    $rootScope.exit = function(){
      Installation.exit();
    }
    
    $rootScope.languages = [
      { id: "en_US.utf8", title: "English US" },
      { id: "id_ID.utf8", title: "Bahasa Indonesia" },
    ];
    
    $rootScope.setLanguage = function(lang) {
      console.log(lang);
      $rootScope.installationData.language = lang.id;
      $rootScope.selectedLang = lang.title;
      $translate.use(lang.id);
      if (window.Installation) {
        Installation.setLocale(lang.id);
      }
    }
    $timeout(function(){
      console.log($(window).width());
      // Fix layout according to screen size
      $(".page").css("width", ($(window).width()*(70/100)).toString() + "px");
      $(".page").css("margin-left", "24px");
      $(".line").css("height", ($(window).height()*(72/100)).toString() + "px");
      $(".line").css("margin-top", ($(window).height()*(10/100)).toString() + "px");
      $(".step-container").css("margin-top", ($(window).height()*(10/100)).toString() + "px");
      $(".step").css("margin-bottom", (($(window).height()*(6.1/100))-10).toString() + "px");
      $(".step-big").css("margin-bottom", (($(window).height()*(10.1/100))-30).toString() + "px");
      $rootScope.started = true;
      $state.go($rootScope.states[$rootScope.currentState]);
    }, 250);
    $timeout(function(){
      $rootScope.showStepLine = true;
    }, 1000);
  }
])


