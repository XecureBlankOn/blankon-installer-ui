angular.module("encryption",[])
.controller("EncryptionCtrl", ["$scope", "$window", "$timeout", "$rootScope", 
  function ($scope, $window, $timeout, $rootScope){
    
    $(".content").css("height", $rootScope.contentHeight);
    
    var currentLang = Installation.getCurrentLanguage();
    for (var i in $rootScope.languages) {
      if ($rootScope.languages[i].id && $rootScope.languages[i].id == currentLang) {
        $rootScope.setLanguage($rootScope.languages[i]);
      }
    }
    
    $rootScope.installationData.secureInstallPassphrase = "";
    $rootScope.installationData.secureInstallPassphraseRepeat = "";

    $scope.title = "Encryption Passphrase";
    
    $scope.validate = function() {
      if (!$rootScope.installationData.secureInstallPassphrase) {
        return;
      } else {
        if ($rootScope.installationData.secureInstallPassphrase != $rootScope.installationData.secureInstallPassphraseRepeat) {
          return;
        }
      }
      $rootScope.next();
    }   
    if ($rootScope.quickDebug) {
      $rootScope.installationData.secureInstallPassphrase = "test";
      $rootScope.installationData.secureInstallPassphraseRepeat = "test";
      setTimeout(function(){
        $scope.setDrive($rootScope.devices[0]);
        $rootScope.next();
      }, 1000);
    }
  }
])
