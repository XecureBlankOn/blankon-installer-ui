angular.module("hello",[])
.controller("HelloCtrl", ["$scope", "$window", "$rootScope", "$translate",
  function ($scope, $window, $rootScope, $translate){
    
    $rootScope.contentHeight = $rootScope.contentHeight || ($(window).height()*(87/100)).toString() + "px"; 
    $(".content").css("height", $rootScope.contentHeight);
    
    if (!$rootScope.isSecurePostInstall) {
      $rootScope.setLanguage($rootScope.languages[0]);
    }
    
    if (window.Installation) {
      $rootScope.release = Installation.getRelease();
      $rootScope.notEfi = parseInt(Installation.isEfi()) > 0 ? false : true;
    }
      
    if ($rootScope.quickDebug) {
      setTimeout(function(){
        $rootScope.next();
      }, 1000);
    }
}])
